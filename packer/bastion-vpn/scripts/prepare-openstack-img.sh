#!/bin/bash

BUILDDIR=./builds/
IMG_MD5_CHECKSUM=4a63e2055a0c3e16e6d26f427f3ec7e9
IMG=coreos_production_openstack_image.img

mkdir -p $BUILDDIR
pushd $BUILDDIR

check_img() {
    [[ "$(md5sum $IMG | awk '{print $1}')" == "$IMG_MD5_CHECKSUM" ]]
}

if [ ! -f ./$IMG ] || ! check_img ; then
    wget https://beta.release.core-os.net/amd64-usr/current/coreos_production_openstack_image.img.bz2 -O - | bzcat > $IMG

    if ! check_img; then
        echo "image is corrupted" >&2
        exit 1
    fi
fi

mkdir -p openstack/latest
cat > openstack/latest/user_data <<EOF
#cloud-config
ssh_authorized_keys:
 - ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ== vagrant insecure public key
EOF

popd
