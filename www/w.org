#+TITLE: What is Blackfish?
#+DATE: 2016/10/05 _ Ippon Technologies 
#+AUTHOR: @yanndegat
#+COMPANY: Ippon Technologies
#+EMAIL: ydegat@ippon.fr
#+LANGUAGE: en
#+WWW: http://blog.ippon.fr/
#+GITLAB: http://gitlab.com/blackfish
#+TWITTER: @yanndegat
#+REVEAL_ROOT: .
#+REVEAL_THEME: black
#+REVEAL_TRANS: slide
#+OPTIONS: toc:1 num:nil

* Blackfish in a nutshell
** docker swarm clusters
- production ready
- on demand
- anywhere

** Build a complete docker swarm infrastructure
#+BEGIN_CENTER
#+ATTR_HTML: :width 300px :align left
[[./images/terraform.png]]
#+ATTR_HTML: :width 300px :align right
[[./images/swarm.png]]
#+END_CENTER

started jan. 16

** Like mantl.io, but for swarm only

Whereas mantl is primarily focused on kubernetes/mesos

** with consul, dns, tls, registry, lb, vpn, metadata, ntp, metrics & log collectors
#+BEGIN_CENTER
#+ATTR_HTML: :width 600px
[[./images/box.svg]]
#+END_CENTER

** multi-cloud & baremetal
#+ATTR_HTML: :width 200px :align left :style margin:15px
[[./images/AWS.png]]
#+ATTR_HTML: :width 200px :align left :style margin:15px
[[./images/logo-ovh.jpg]]
#+ATTR_HTML: :width 200px :align right :style margin:15px
[[./images/openstack.png]]
#+ATTR_HTML: :width 200px :align right :style margin:15px
[[./images/coreos_logo.png]]

** and a focus on usability
- stick to the docker CLI
- + goodies

#+BEGIN_CENTER
#+ATTR_HTML: :height 200px
[[./images/usability.svg]]
#+END_CENTER

* Usability
** Provision a  Cassandra+Spark cluster on a private network with DataMC ! !!
#+BEGIN_CENTER
#+ATTR_HTML: :height 500px
[[./images/simple_cluster_ovh.svg]]
#+END_CENTER
** 
#+BEGIN_CENTER
#+ATTR_HTML: :height 600px
[[./images/complex_cluster.svg]]
#+END_CENTER

** easy to grow an existing infrastructure

#+BEGIN_SRC bash
$ consul agent -join XX.XY.YY.ZZ ...
#+END_SRC

** easy but  secured access to the docker engine
- No public endpoint exposed
- Firewall rules
- Per user TLS certs

* Roadmap
+ ..... *InfraKit* ......
+ docker 1.12.?
+ output a complete terraform project
+ docker-machine plugin
+ pki infrastructure
+ vault
+ a good docker volume driver (rexray, flocker, convoy, contiv... ?) to ease node replacements


* Thank you
