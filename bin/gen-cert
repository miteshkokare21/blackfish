#!/bin/bash
BASEDIR=$(readlink "$(dirname "$0")")
STACK="$1"
CN="$2"
HOSTNAME="$3"

CERTDIR=${CERTDIR:-"$HOME/.blackfish/$STACK"}

if [ -z "$STACK" ]; then
    echo "usage: $BASEDIR/blackfish.sh STACK CN [HOSTNAME]"
    echo "It will generate  node cert in the current directory from the CA cert in the $CERTDIR dir."
    exit 1
fi

cat > "$(pwd)"/run.sh <<EOF
pushd $(pwd)
echo '{"signing":{"default":{"expiry":"8760h","usages":["signing","server auth"]}}}' > req.json
if [ ! -z "$HOSTNAME" ]; then
   echo '{"CN":"$CN","names":[{"OU":"$STACK"}],"hosts":["$HOSTNAME"],"key":{"algo":"rsa","size":2048}}' | cfssl gencert -ca=$CERTDIR/ca.pem -ca-key=$CERTDIR/ca-key.pem -config=req.json -hostname $HOSTNAME - | cfssljson -bare "$CN"
else
   echo '{"CN":"$CN","names":[{"OU":"$STACK"}],"key":{"algo":"rsa","size":2048}}' | cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=req.json - | cfssljson -bare "$CN"
fi
popd
EOF

chmod +x "$(pwd)/run.sh"

which cfssl > /dev/null 2>&1
if [ $? -eq 0 ]; then
    exec "$(pwd)/run.sh"
else
    which docker > /dev/null 2>&1
    if [ $? -eq 0 ] && [ -S /var/run/docker.sock ]; then
        docker run --rm -v "$(pwd)":"$(pwd)" -u $(id -u):$(id -g) --entrypoint /bin/bash cfssl/cfssl -c "$(pwd)/run.sh"
    else
        echo "couldn't find nor cfssl nor docker binary." >&2
        exit 1
    fi
fi

find "$(pwd)" -type f -exec chmod 0600 {} \;
rm -f "$(pwd)/run.sh $(pwd)/req.json"
