#!/bin/bash
BASEDIR=$(readlink "$(dirname "$0")")
print_opts=0

# Usage info
show_help() {
    cat << EOF
Usage: ${0##*/} [-ho] STACK_NAME
runs docker command targeting a swarm cluster

OPTIONS:
    -h                 display this help and exit
    -o                 only outputs docker engine opts
EOF
}


OPTIND=1 # Reset is necessary if getopts was used previously in the script.  It is a good idea to make this local in a function.
while getopts ":ho" opt; do
    case "$opt" in
        h)
            show_help
            exit 0
            ;;
        o)  print_opts=1
            ;;
        '?')
            show_help >&2
            exit 1
            ;;
    esac
done

shift "$((OPTIND-1))" # Shift off the options and optional --.

STACK="${1%:*}"
# $1 contains : extract the datacenter
if [[ $1 == *":"* ]]
then
  DATACENTER="${1#*:}"
fi

shift

if [ -z "$STACK" ]; then
    echo "usage: $BASEDIR/blackfish.sh STACK[:DC]"
fi

if [ ! -d "$HOME/.blackfish/$STACK" ]; then
    echo "$STACK is not present in $HOME/.blackfish. Generate the certificates with:" >&2
    echo "$BASEDIR/certs.sh $STACK" >&2
    exit 1
fi

if [ ! -z "$DATACENTER" ]; then
  HOST="tcp://swarm.service.$DATACENTER.$STACK:4000"
else
  HOST="tcp://swarm.service.$STACK:4000"
fi

if [[ "$print_opts" == 1 ]]; then
    printf -- "-H %s --tlsverify --tlscacert %s --tlscert %s --tlskey %s" "$HOST" "$HOME/.blackfish/$STACK/ca.pem" "$HOME/.blackfish/$STACK/cert.pem" "$HOME/.blackfish/$STACK/key.pem"
else
    DOCKER_API_VERSION=1.23 docker -H "$HOST" --tlsverify \
           --tlscacert "$HOME/.blackfish/$STACK/ca.pem" \
           --tlscert "$HOME/.blackfish/$STACK/cert.pem" \
           --tlskey "$HOME/.blackfish/$STACK/key.pem" \
           "$@"
fi
